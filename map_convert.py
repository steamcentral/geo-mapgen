#!/usr/bin/env python3

# This script is made to convert a Digital Elevation Model image 
# (usually GeoTIFF) into a database, readable by Minetest to generate 
# real terrain via command line or script

import map_transform
import database
import rivers

from landcover import make_landcover

import click

@click.command()
@click.argument("heights", type=click.Path(exists=True))
@click.argument("worldpath", type=click.Path(exists=True))
@click.option("--east", help="eastern boundary",
              default=None,
              type=click.FLOAT)
@click.option("--west", help="western boundary",
              default=None,
              type=click.FLOAT)
@click.option("--north", help="northern boundary",
              default=None,
              type=click.FLOAT)
@click.option("--south", help="southern boundary",
              default=None,
              type=click.FLOAT)
@click.option("--tilesize",
              help="Horizontal tiles size",
              default=80,
              type=click.IntRange(0, 1024))
@click.option("--scale",
              default=40,
              type=click.IntRange(0, 1000),
              help="Vertical scale in meters per node")
@click.option("--landcovermap",
              default=None,
              type=click.Path(exists=True),
              help="land cover map")
@click.option("--landcoverlegend",
              default=None,
              type=click.Path(exists=True),
              help="land cover legend")
@click.option("--rivermap",
              default=None,
              type=click.Path(exists=True),
              help="river map")
@click.option('--genrivers/--no-genrivers', default=False)
@click.option("--riverlimit",
              default=1000,
              type=click.FLOAT,
              help="Minimal drainage basin")
@click.option("--riverhdiff",
              default=40,
              type=click.FLOAT,
              help="Maximal height difference")
@click.option("--riverpower",
              default=0.25,
              type=click.FLOAT,
              help="River widening power")
@click.option("--sealevel",
              default=-128,
              type=click.FLOAT,
              help="Sea level")
@click.option("--spawnx",
              default=128,
              type=click.FLOAT,
              help="first spawn X coordinate")
@click.option("--spawny",
              default=128,
              type=click.FLOAT,
              help="first spawn Y coordinate")
@click.option("--spawnz",
              default=-128,
              type=click.FLOAT,
              help="first spawn Z coordinate")


def mapgen(heights, worldpath, east, west, north, south, tilesize, scale, landcovermap, landcoverlegend, rivermap, genrivers, riverlimit, riverhdiff, riverpower, sealevel, spawnx, spawny, spawnz):

    hmap_data = worldpath + "/heightmap.dat"
    hmap_conf = worldpath + "/heightmap.dat.conf"

    # Load files at the beginning, so that if a path is wrong, the user will know it instantly.
    file_output = open(hmap_data, "wb")
    file_conf = open(hmap_conf, "w")

    map_transform.update_map("heightmap", heights)
    heightmap = map_transform.read_map("heightmap", interp=4) # Read with Lanczos interpolation (code 4)
    hmap_north, hmap_east, hmap_south, hmap_west = map_transform.get_map_bounds("heightmap")

    crop_map = False
    reproject_map = False

    want_east = hmap_east
    want_west = hmap_west
    want_north = hmap_north
    want_south = hmap_south

    if east != None:
        want_east = east
        crop_map = True

    if west != None:
        want_west = west
        crop_map = True

    if north != None:
        want_north = north
        crop_map = True

    if south != None:
        want_south = south
        crop_map = True

    if tilesize != None:
        reproject_map = True

    if crop_map or reproject_map:
        map_transform.set_parameters(reproject=reproject_map, crop=True, region=(want_north, want_east, want_south, want_west), hscale=tilesize)
    else:
        map_transform.set_parameters(reproject=False, crop=False, reference="heightmap")

    npx, npy, _, _, _ = map_transform.get_map_size()

    print("world is {:d} x {:d} tiles".format(int(npx), int(npy)))


    if spawnx is None:
        spawnx = npx // 2

    if spawnz is None:
        spawnz = npy // 2

    if spawny is None:
        spawny = 4


    normalized_rivermap = None

    if rivermap != None:
        map_transform.update_map("rivers", rivermap)
        normalized_rivermap = map_transform.read_map("rivers", interp=8)
    else:
        if genrivers:
            normalized_rivermap = rivers.generate_rivermap(heightmap, sea_level=sealevel, river_limit=riverlimit, river_power=riverpower)

    normalized_landmap = None
    legend = None

    if landcovermap != None:
        map_transform.update_map("landcover", landcovermap)
        landmap_raw = map_transform.read_map("landcover", interp=0)
        normalized_landmap, legend = make_landcover(landmap_raw, landcoverlegend)

    database.generate(file_output, file_conf, heightmap,
                      rivermap=normalized_rivermap,
                      landmap=normalized_landmap,
                      landmap_legend=legend,
                      frag=tilesize,
                      scale=scale,
                      region=map_transform.param_region,
                      spawn_point=[spawnx, spawny, spawnz])
    pass


if __name__ == "__main__":
    mapgen()

